$(function ($) {  

    $.fn.i18n = function(options){
        
        var el = $(this).length > 0 ? $(this) : $('html');
        var settings = $.extend({
            lang: "pt-BR",
        }, options );

        $.getJSON(_URL + "i18n?lang=" + settings.lang, function(messagesJson) {
            
            translateElements(el, messagesJson);
        });
    } 

    function translateElements(el, messagesJson) {
        
        var elements = $(el).find('[data-i18n]');

        elements.each(function(){

            var param = $(this).attr("data-i18n");
            var keys = param.split('.');

            var msg = messagesJson;
            $.each(keys, function(i, key){

                msg = msg[key];
            });

            var el = $(this);
            if (el.context.tagName == 'INPUT' && el.context.type != 'button') 
                el.attr('placeholder', msg);

            else if (el.context.tagName == 'INPUT')
                el.val(msg);

            else 
                el.text(msg);
        });
    }

}( jQuery ));
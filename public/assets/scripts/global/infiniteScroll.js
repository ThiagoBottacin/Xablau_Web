 //Infinite Scroll controls
 var ignoreScroll = false;
 var registersPerPage;
 var currentPage = 1;
 var ajaxScroll;
 var limitPage;
 var func;

$(function(){

    $(window).scroll(function() {
        if (ignoreScroll) return;
    
        if ((($(window).scrollTop() + $(window)[0].outerHeight) >= $(document).height()) && currentPage < limitPage) {
            
            ignoreScroll = true;
            ajaxScroll.abort();
            
            if(typeof(func) === "function")
                func(++currentPage);
        }
    });

});


function bindScroll(){

    setTimeout(function(){    

        ignoreScroll = false;    
    }, 1000);
}


function infiniteScroll(targetFunction, pageRegisters = 12){
    
    currentPage = 1;
    registersPerPage = pageRegisters;

    func = targetFunction;
}

function scrollinitialize(totRegisters){

    limitPage = Math.ceil(totRegisters / registersPerPage);
}

function setRegistersPerPage(value){

    registersPerPage = value;
}

function setCurrentPage(value){
    
    currentPage = value;
}

function removeInfiniteScroll(){
    func = null;
}

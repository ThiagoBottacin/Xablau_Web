﻿
console.debug('myGrid');

function clickPage(element, max) {
    var index = $(element).attr('data-goto');
    var value = parseInt(page_element.find('[style]').attr('page'));//parseInt($(element).attr('page'));
    //var max = $(page_element).find('a[data-goto="page"]').length;
    if (index == "page") {
        current_page = parseInt($(element).attr('page'));
    }
    else if (index == "first") {
        current_page = 1;
    }
    else if (index == "prev" && value > 1) {
        current_page = value - 1;
    }
    else if (index == "next" && value < max) {
        current_page = value + 1;
    }
    else if (index == "last") {
        current_page = max;
    }
    banco_dados = getDataDB(term, current_page, selects_term);
    buildGrid(banco_dados);
}

function buildPages(n_pages) {
    page_element.append('<li><a aria-label="Primeira" data-goto="first" onclick="clickPage(this)"><span aria-hidden="true">&laquo;</span></a></li>');
    page_element.append('<li><a aria-label="Anterior" data-goto="prev" onclick="clickPage(this,' + n_pages + ')"><span aria-hidden="true">&lt;</span></a></li>');

    var min_page;
    var max_page;
    
    if (current_page <= 3) {
        min_page = 1;
        max_page = (n_pages<5)?n_pages:5;
    }
    if(n_pages>5){
        if (current_page > 3 && current_page < n_pages-2) {
            min_page = current_page - 2;
            max_page = current_page + 2;
        }
        else if(current_page >= n_pages - 2){
            min_page = n_pages-4;
            max_page = n_pages;
        }
    }

    for (var i = min_page; i <= max_page; i++) {
        var number = $('<li><a aria-label="Page" data-goto="page" page="' + i + '" onclick="clickPage(this)"><span aria-hidden="true">' + i + '</span></a></li>');
        page_element.append(number);
    }

    page_element.append('<li><a aria-label="Próxima" data-goto="next" onclick="clickPage(this,' + n_pages + ')"><span aria-hidden="true">&gt;</span></a></li>');
    page_element.append('<li><a aria-label="Última" data-goto="last" onclick="clickPage(this,' + n_pages + ')"><span aria-hidden="true">&raquo;</span></a></li>');
}

function colorPageSelected(n_page) {
    $(page_element).find('a[page]').removeAttr('style');
    $(page_element).find('a[page="' + n_page + '"]').css('background', '#90a4ae').css('color', 'white');
}

function resetPagination() {
    $(page_element).children().each(function () {
        $(this).remove();
    });
}

function resetGrid() {
    $(grid_body).children().each(function () {
        $(this).remove();
    });
}

function colorRowSelectted(element) {   
    var row = $(element).parent().parent();
    row.css('background', '#eeb');
}

function resetColorRowSelected() {
    $(grid_body).children().each(function () {
        $(this).removeAttr('style');
    });
}

////////////////////////////-/

function buildPages2(page_element) {

    var current_page = $(page_element).attr('currentPage');
    var n_pages = $(page_element).attr('numberOfPages');

    page_element.append('<li><a aria-label="Primeira" data-goto="first" onclick="clickPage2(this,' + n_pages + ')"><span aria-hidden="true">&laquo;</span></a></li>');
    page_element.append('<li><a aria-label="Anterior" data-goto="prev" onclick="clickPage2(this,' + n_pages + ')"><span aria-hidden="true">&lt;</span></a></li>');

    appendNumberPages(page_element, current_page, n_pages);

    page_element.append('<li><a aria-label="Próxima" data-goto="next" onclick="clickPage2(this,' + n_pages + ')"><span aria-hidden="true">&gt;</span></a></li>');
    page_element.append('<li><a aria-label="Última" data-goto="last" onclick="clickPage2(this,' + n_pages + ')"><span aria-hidden="true">&raquo;</span></a></li>');
}

function appendNumberPages(page_element, current_page, n_pages) {
    var min_page;
    var max_page;

    var current_page = parseInt(current_page);

    if (current_page <= 3) {
        min_page = 1;
        max_page = (n_pages < 5) ? n_pages : 5;
    }
    if (n_pages > 5) {
        if (current_page > 3 && current_page < n_pages - 2) {
            min_page = current_page - 2;
            max_page = current_page + 2;
        }
        else if (current_page >= n_pages - 2) {
            min_page = n_pages - 4;
            max_page = n_pages;
        }
    }
   
    for (var i = min_page; i <= max_page; i++) {
        var number = $('<li><a aria-label="Page" data-goto="page" page="' + i + '" onclick="clickPage2(this,' + n_pages + ')"><span aria-hidden="true">' + i + '</span></a></li>');
        page_element.append(number);
    }
}

function clickPage2(page_element, max) {

    debugger;

    var index = $(page_element).attr('data-goto');
    var c_page = parseInt($(page_element).parent().parent().find('[style]').attr('page'));

    if (index == "page") {
        c_page = parseInt($(page_element).attr('page'));
    }
    else if (index == "first") {
        c_page = 1;
    }
    else if (index == "prev" && c_page > 1) {
        c_page = c_page - 1;
    }
    else if (index == "next" && c_page < max) {
        c_page = c_page + 1;
    }
    else if (index == "last") {
        c_page = max;
    }

    current_pageexam = c_page;
    loadPaginationExame();

    //colorPageSelected2(c_page, $(page_element).parent().parent());
    //showExamByPagination(c_page);   
}

function colorPageSelected2(c_page, page_element) {
    page_element.find('a[page]').removeAttr('style');
    page_element.find('a[page="' + c_page + '"]').css('background', '#90a4ae').css('color', 'white');
    page_element.attr('currentPage', c_page);
}

//////////////////////////////-/

function buildPages3(page_element) {

    var current_page = $(page_element).attr('currentPage');
    var n_pages = $(page_element).attr('numberOfPages');

    page_element.append('<li><a aria-label="Primeira" data-goto="first" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&laquo;</span></a></li>');
    page_element.append('<li><a aria-label="Anterior" data-goto="prev" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&lt;</span></a></li>');

    appendNumberPages3(page_element, current_page, n_pages);

    page_element.append('<li><a aria-label="Próxima" data-goto="next" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&gt;</span></a></li>');
    page_element.append('<li><a aria-label="Última" data-goto="last" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&raquo;</span></a></li>');
}

function appendNumberPages3(page_element, current_page, n_pages) {
    var min_page;
    var max_page;

    var current_page = parseInt(current_page);

    if (current_page <= 5) {
        min_page = 1;
        max_page = (n_pages < 5) ? n_pages : 5;
    }
    if (n_pages > 5) {
        if (current_page > 3 && current_page < n_pages - 2) {
            min_page = current_page - 2;
            max_page = current_page + 2;
        }
        else if (current_page >= n_pages - 2) {
            min_page = n_pages - 4;
            max_page = n_pages;
        }
    }

    for (var i = min_page; i <= max_page; i++) {
        var number = $('<li><a aria-label="Page" data-goto="page" page="' + i + '" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">' + i + '</span></a></li>');
        page_element.append(number);
    }
}

function clickPage3(page_element, max) {
    var index = $(page_element).attr('data-goto');
    var c_page = parseInt($(page_element).parent().parent().find('[style]').attr('page'));

    if (index == "page") {
        c_page = parseInt($(page_element).attr('page'));
    }
    else if (index == "first") {
        c_page = 1;
    }
    else if (index == "prev" && c_page > 1) {
        c_page = c_page - 1;
    }
    else if (index == "next" && c_page < max) {
        c_page = c_page + 1;
    }
    else if (index == "last") {
        c_page = max;
    }
    $(page_element).parents('.pagination').attr('currentPage', c_page);
}

//////////////////////////////-/

function buildPages3(page_element) {

    var current_page = $(page_element).attr('currentPage');
    var n_pages = $(page_element).attr('numberOfPages');

    page_element.append('<li><a aria-label="Primeira" data-goto="first" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&laquo;</span></a></li>');
    page_element.append('<li><a aria-label="Anterior" data-goto="prev" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&lt;</span></a></li>');

    appendNumberPages3(page_element, current_page, n_pages);

    page_element.append('<li><a aria-label="Próxima" data-goto="next" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&gt;</span></a></li>');
    page_element.append('<li><a aria-label="Última" data-goto="last" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">&raquo;</span></a></li>');
}

function appendNumberPages3(page_element, current_page, n_pages) {
    var min_page;
    var max_page;

    var current_page = parseInt(current_page);
    if (current_page <= 5) {
        min_page = 1;
        max_page = (n_pages < 5) ? n_pages : 5;
    }
    if (n_pages > 5) {
        if (current_page > 3 && current_page < n_pages - 2) {
            min_page = current_page - 2;
            max_page = current_page + 2;
        }
        else if (current_page >= n_pages - 2) {
            min_page = n_pages - 4;
            max_page = n_pages;
        }
    }

    for (var i = min_page; i <= max_page; i++) {
        var number = $('<li><a aria-label="Page" data-goto="page" page="' + i + '" onclick="clickPage3(this,' + n_pages + ')"><span aria-hidden="true">' + i + '</span></a></li>');
        page_element.append(number);
    }
}

function clickPage3(page_element, max) {
    var index = $(page_element).attr('data-goto');
    var c_page = parseInt($(page_element).parent().parent().find('[style]').attr('page'));

    if (index == "page") {
        c_page = parseInt($(page_element).attr('page'));
    }
    else if (index == "first") {
        c_page = 1;
    }
    else if (index == "prev" && c_page > 1) {
        c_page = c_page - 1;
    }
    else if (index == "next" && c_page < max) {
        c_page = c_page + 1;
    }
    else if (index == "last") {
        c_page = max;
    }
    $(page_element).parents('.pagination').attr('currentPage', c_page);
}

//////////////////////////////-/

function buildPages4(page_element) {

    var current_page = $(page_element).attr('currentPage');
    var n_pages = $(page_element).attr('numberOfPages');

    page_element.append('<li><a aria-label="Primeira" data-goto="first" onclick="clickPage4(this,' + n_pages + ')"><span aria-hidden="true">&laquo;</span></a></li>');
    page_element.append('<li><a aria-label="Anterior" data-goto="prev" onclick="clickPage4(this,' + n_pages + ')"><span aria-hidden="true">&lt;</span></a></li>');

    appendNumberPages4(page_element, current_page, n_pages);

    page_element.append('<li><a aria-label="Próxima" data-goto="next" onclick="clickPage4(this,' + n_pages + ')"><span aria-hidden="true">&gt;</span></a></li>');
    page_element.append('<li><a aria-label="Última" data-goto="last" onclick="clickPage4(this,' + n_pages + ')"><span aria-hidden="true">&raquo;</span></a></li>');
}

function colorPageSelected4(c_page, page_element) {
    page_element.find('a[page]').removeAttr('style');
    page_element.find('a[page="' + c_page + '"]').css('background', '#90a4ae').css('color', 'white');
    page_element.attr('currentPage', c_page);
}

function clickPage4(page_element, max) {
    var index = $(page_element).attr('data-goto');
    var c_page = parseInt($(page_element).parent().parent().find('[style]').attr('page'));

    if (index == "page") {
        c_page = parseInt($(page_element).attr('page'));
    }
    else if (index == "first") {
        c_page = 1;
    }
    else if (index == "prev" && c_page > 1) {
        c_page = c_page - 1;
    }
    else if (index == "next" && c_page < max) {
        c_page = c_page + 1;
    }
    else if (index == "last") {
        c_page = max;
    }

    current_pagespec = c_page;
    loadPaginationEspecialidade();
}

function appendNumberPages4(page_element, current_page, n_pages) {
    var min_page;
    var max_page;

    var current_page = parseInt(current_page);

    if (current_page <= 3) {
        min_page = 1;
        max_page = (n_pages < 5) ? n_pages : 5;
    }
    if (n_pages > 5) {
        if (current_page > 3 && current_page < n_pages - 2) {
            min_page = current_page - 2;
            max_page = current_page + 2;
        }
        else if (current_page >= n_pages - 2) {
            min_page = n_pages - 4;
            max_page = n_pages;
        }
    }

    for (var i = min_page; i <= max_page; i++) {
        var number = $('<li><a aria-label="Page" data-goto="page" page="' + i + '" onclick="clickPage4(this,' + n_pages + ')"><span aria-hidden="true">' + i + '</span></a></li>');
        page_element.append(number);
    }
}


////////////////////////////-/

function buildPages5(page_element) {

    var current_page = $(page_element).attr('currentPage');
    var n_pages = $(page_element).attr('numberOfPages');

    page_element.append('<li><a aria-label="Primeira" data-goto="first" onclick="clickPage25this,' + n_pages + ')"><span aria-hidden="true">&laquo;</span></a></li>');
    page_element.append('<li><a aria-label="Anterior" data-goto="prev" onclick="clickPage5(this,' + n_pages + ')"><span aria-hidden="true">&lt;</span></a></li>');

    appendNumberPages5(page_element, current_page, n_pages);

    page_element.append('<li><a aria-label="Próxima" data-goto="next" onclick="clickPage5(this,' + n_pages + ')"><span aria-hidden="true">&gt;</span></a></li>');
    page_element.append('<li><a aria-label="Última" data-goto="last" onclick="clickPage5(this,' + n_pages + ')"><span aria-hidden="true">&raquo;</span></a></li>');
}

function appendNumberPages5(page_element, current_page, n_pages) {
    var min_page;
    var max_page;

    var current_page = parseInt(current_page);

    if (current_page <= 3) {
        min_page = 1;
        max_page = (n_pages < 5) ? n_pages : 5;
    }
    if (n_pages > 5) {
        if (current_page > 3 && current_page < n_pages - 2) {
            min_page = current_page - 2;
            max_page = current_page + 2;
        }
        else if (current_page >= n_pages - 2) {
            min_page = n_pages - 4;
            max_page = n_pages;
        }
    }

    for (var i = min_page; i <= max_page; i++) {
        var number = $('<li><a aria-label="Page" data-goto="page" page="' + i + '" onclick="clickPage5(this,' + n_pages + ')"><span aria-hidden="true">' + i + '</span></a></li>');
        page_element.append(number);
    }
}

function clickPage5(page_element, max) {

    debugger;

    var index = $(page_element).attr('data-goto');
    var c_page = parseInt($(page_element).parent().parent().find('[style]').attr('page'));

    if (index == "page") {
        c_page = parseInt($(page_element).attr('page'));
    }
    else if (index == "first") {
        c_page = 1;
    }
    else if (index == "prev" && c_page > 1) {
        c_page = c_page - 1;
    }
    else if (index == "next" && c_page < max) {
        c_page = c_page + 1;
    }
    else if (index == "last") {
        c_page = max;
    }

    current_pageChuva = c_page;
    loadPaginationChuva();


}

function colorPageSelected5(c_page, page_element) {
    page_element.find('a[page]').removeAttr('style');
    page_element.find('a[page="' + c_page + '"]').css('background', '#90a4ae').css('color', 'white');
    page_element.attr('currentPage', c_page);
}

//Gera Paginação Genérica
function loadPagination(page_element, total, elementsByPage) {
    $(page_element).children().remove();

    // var elementsByPage = 10;
    var numberOfPages = Math.ceil(total / elementsByPage);

    if (numberOfPages == 0) {
        numberOfPages == 1;
    }
    $(page_element).attr('numberOfPages', numberOfPages);
    var current_page = $(page_element).attr('currentPage');
    buildPages3(page_element);
    colorPageSelected2(current_page, page_element);
}

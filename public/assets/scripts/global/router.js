$(document).ready(function(){

    $('html').on('click', '.router', function(){

        //Router
        var router = $(this);
        var route = router.attr('data-target');
        
        $('.router').parent().removeClass('active');
        if ($('.sideBar[data-target='+ route +']').length > 0) {
            
            $('.sideBar[data-target='+ route +']').parent().addClass('active');
        } else if (route === "xablauDetailsArea"){

            $('.sideBar[data-target=xablausArea]').parent().addClass('active');
        }
        
        //Routes
        var home = $('#homeArea');
        var managementArea = $('#managementArea');
        var profile = $('#profileArea');
        var addresses = $('#addressesArea');
        var items = $('#itemsArea');
        var xablaus = $('#xablausArea');
        var xablauDetails = $('#xablauDetailsArea');


        home.hide();
        profile.hide();
        addresses.hide();
        items.hide();
        xablaus.hide();
        xablauDetails.hide();

        if (route !== 'homeArea') 
        managementArea.show();    
        else 
            managementArea.hide();

        //Show Route
        $('#'+route).show();
        

        switch (route) {
            case 'homeArea':
                
                home_pageLoad();
                break;
            case 'profileArea':
                
                profile_pageLoad();
                break;
            case 'addressesArea':
                
                address_pageLoad();
                break;
            case 'itemsArea':
                
                items_pageLoad();
                break;
            case 'xablausArea':
                
                xablaus_pageLoad();
                break;
            case 'xablauDetailsArea':
                
                var changeId = router.attr('data-change');
                var itemId = router.attr('data-id');
                getXablauDetails(changeId, itemId);
                break;
            case 'searchArea':
                
                //TODO
                break;
            default:
                
                home_pageLoad();
                break;
        }
    });
});

﻿$(document).ready(function () {
    
    $.fn.i18n({lang: "pt-BR"});
    
    home_pageLoad();        
});


function home_pageLoad(){

    verificaAuth();

    if ($('#homeArea').is(':visible')) {
        
        verificaAuth();
        removeInfiniteScroll();

        showLoader(true);

        //Preenche areas com os produtos
        pegarProdutosMatchs();
        pegarProdutosInteressados();
        homeControlsInicialize();
    }
}


function pegarProdutosMatchs() {

    $.ajax({
        type: "GET",
        url: _URL + "change/matchs",
        dataType: "json",
        async: true,
        headers: {
            authorization: window.token
        },
        success: function (response) {

            $('#myCarousel').slick('removeSlide', null, null, true);

            if (response.length > 0) {

                $('#homeArea_dvAlertaProduto').hide();
                $('#dvMensagemTopo').show();

                for (var i = 0; i < response.length; i++) {

                    var item = response[i];

                    if (item.UrlPhotos == null || item.UrlPhotos == "" || item.UrlPhotos == undefined) {
                        item.UrlPhotos[0] == "../../assets/img/800x600.png";
                    }

                    $('#myCarousel').slick('slickAdd', '<div class="col-md-4 card_produto" data-id="' + item.ItemId + '" data-change="' + item.ChangeId + '">' +
                        '<div class="product-thumb my-card-carousel">' +
                        '<header class="product-header">' +
                        '<img id="imgProduto" alt="Imagem do produto" src="' + item.UrlPhotos[0] + '" title="Imagem do produto" style="max-height:200px; min-height:200px;" />' +
                        '<div class="product-quick-view">' +
                        '<a id="detalhes-produto" class="fa fa-eye popup-text detalhes-produto" title="Mais Detalhes"></a>' +
                        '</div>' +
                        '</header>' +
                        '<div class="product-inner">' +
                        '<h5 class="product-title">' + item.Name + '</h5>' +
                        '<div class="product-meta">' +
                        '<ul class="product-price-list">' +
                        '<li>' +
                        '<span class="product-price">' + item.Price + '</span>' +
                        '</li>' +
                        '</ul>' +
                        '<ul class="product-actions-list">' +
                        '<li>' +
                        '<a class="btn btn-sm to-afim"><i class="fa fa-thumbs-o-up"></i> Tô Afim</a>' +
                        '</li>' +
                        '<li>' +
                        '<a class="btn btn-sm to-fora"><i class="fa fa-thumbs-o-down"></i> Tô Fora</a>' +
                        '</li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                }

                addMaskMoney();
            }
            else {
                $('#dvMensagemTopo').hide();
                $('#homeArea_dvAlertaProduto').show();
            }
        },
        complete: function (){
            
            showLoader(false);
        }
    });
}

function pegarProdutosInteressados() {
    $.ajax({
        type: "GET",
        url: _URL + "items/interested",
        dataType: "json",
        async: true,
        headers: {
            authorization: window.token
        },
        success: function (response) {

            $('#interessadosCarousel').slick('removeSlide', null, null, true);

            if (response.length > 0) {

                $('#dvAlertaInteressado').hide();
                $('#dvFraseInteressados').show();
                

                for (var i = 0; i < response.length; i++) {

                    var item = response[i];

                    if (item.UrlPhotos == null || item.UrlPhotos == "" || item.UrlPhotos == undefined) {
                        item.UrlPhotos[0] == "../../assets/img/800x600.png";
                    }

                    debugger;
                    $('#interessadosCarousel').slick('slickAdd', '<div class="col-md-4 card_produto" data-id="' + item.ItemId +'">' +
                        '<div class="product-thumb my-card-carousel">' +
                        '<header class="product-header">' +
                        '<img id="imgProduto" alt="Imagem do produto" src="' + item.UrlPhotos[0] + '" title="Imagem do produto" style="max-height:200px; min-height:200px;" />' +
                        '<div class="product-quick-view">' +
                        '<a id="detalhes-produto" class="fa fa-eye popup-text detalhes-produto" title="Mais Detalhes"></a>' +
                        '</div>' +
                        '</header>' +
                        '<div class="product-inner">' +
                        '<h5 class="product-title">' + item.Name + '</h5>' +
                        '<div class="product-meta">' +
                        '<ul class="product-price-list">' +
                        '<li>' +
                        '<span class="product-price">' + item.Price + '</span>' +
                        '</li>' +
                        '</ul>' +
                        '<ul class="product-actions-list">' +
                        '<li>' +
                        '<a class="btn btn-sm to-afim"><i class="fa fa-thumbs-o-up"></i> Tô Afim</a>' +
                        '</li>' +
                        '<li>' +
                        '<a class="btn btn-sm to-fora"><i class="fa fa-thumbs-o-down"></i> Tô Fora</a>' +
                        '</li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                }

                addMaskMoney();
            }
            else {
                $('#dvFraseInteressados').hide();
                $('#dvAlertaInteressado').show();
            }
        },
        complete: function (){
            
            showLoader(false);
        }
    });
}

//function To Afim e To Fora
function like(like, product, change) {

    var data = {
        ChangeId: change,
        ProductId: product,
        Confirmation: like
    }

    $.ajax({
        type: "POST",
        url: _URL + "change/like",
        headers: {
            authorization: window.token
        },
        async: true,
        data: data,
        success: function (response) {

            //TODO
            debugger;
            if (response) {
                swal({
                    
                    title: 'Você tem um novo xablau!',
                    imageUrl: '../../assets/img/logos/Logo_Xablau.png',
                    imageWidth: 174,
                    imageHeight: 200,
                    animation: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    showCancelButton: true,
                    confirmButtonText: 'Meus Xablaus',
                    cancelButtonText: 'Continuar',
                    buttonsStyling: true

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        
                        $('.router[data-target=xablausArea]').trigger('click');
                    }
                });
            }
        },
        error: function (response) {
            swal(
                'Oops...',
                response.responseText,
                'error'
            );
        }
    });
}

function homeControlsInicialize() {

    //Carousel Combinações
    $('#myCarousel').not('.slick-initialized').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

    //Carousel Produtos Interessados
    $('#interessadosCarousel').not('.slick-initialized').slick({
        slidesToShow: 4,
        slidesToScroll: 3,
        infinite: false,
        arrows: true
    });

    homeArea_addSlickImgDetails();

    $('.card_produto').expire();
    //Funções dos Cards dos Produtos
    $('.card_produto').livequery(function () {

        var cardProduto = $(this);
        var $like = $(this).find('.to-afim');
        var $deslike = $(this).find('.to-fora');
        var $details = $(this).find('.fa-eye');

        var itemId = cardProduto.attr('data-id');
        var changeId = cardProduto.attr('data-change');
        var slickId = cardProduto.attr("data-slick-index");

        $like.click(function () {

            like(true, itemId, changeId);
            removeItemSlick(slickId, changeId);
        });

        $deslike.click(function () {

            like(false, itemId, changeId)
            removeItemSlick(slickId, changeId);
        });

        $details.click(function () {

            itemDetails(itemId);
        });
    });

    addMaskMoney();
}

function removeItemSlick(slickId, changeId) {
    
    debugger;
    var cardCount;
    if (changeId != undefined) {

        $('#myCarousel').slick('slickRemove', slickId);

        cardCount = $('#myCarousel').slick('getSlick').slideCount;
        if (cardCount <= 0) {
            $('#dvMensagemTopo').hide();
            $('#homeArea_dvAlertaProduto').show();
        }
    }
    else {

        $('#interessadosCarousel').slick('slickRemove', slickId);

        cardCount = $('#interessadosCarousel').slick('getSlick').slideCount;
        if (cardCount <= 0) {
            $('#dvFraseInteressados').hide();
            $('#dvAlertaInteressado').show();
        }

    }
}

//Adiciona máscara de dinheiro
function addMaskMoney() {

    //Adiciona mascara de Valor (R$)
    $('.product-price').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
}

//Adiciona plugin slick nas fotos do modal Details
function homeArea_addSlickImgDetails() {

    //Imagens do modal Detalhes
    $('#homeArea_detailsMainImage').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#homeArea_detailsImages'
    });
    $('#homeArea_detailsImages').not('.slick-initialized').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        asNavFor: '#homeArea_detailsMainImage'
    });
}

//Monta modal detalhes
function itemDetails(itemId) {

    //Limpa Campos
    $('#homeArea_detailsName').val("");
    $('#homeArea_detailsPrice').val("");
    $('#homeArea_detailsDescription').val("");
    $('#homeArea_detailsAddress').val("");

    //Limpa as Tags já inseridas
    $('#homeArea_detailsTag').empty();

    //Limpa as Fotos já inseridas
    $('#homeArea_detailsMainImage').slick('unslick');
    $('#homeArea_detailsImages').slick('unslick');
    $('#homeArea_detailsMainImage').empty();
    $('#homeArea_detailsImages').empty();

    $.ajax({
        type: "GET",
        url: _URL + "items/"+itemId+"/details",
        headers: {
            authorization: window.token
        },
        success: function (response) {

            var item = response[0];

            $('#homeArea_detailsName').text(item.Name);
            $('#homeArea_detailsPrice').text(item.Price);
            $('#homeArea_detailsDescription').text(item.Description);
            $('#homeArea_detailsAddress').text(item.Address != null && item.Address != undefined ? item.Address : "Endereço não cadastrado");

            for (var i = 0; i < item.WishTag.length; i++) {
                $('#homeArea_detailsTag').append('<li><i class="fa fa-tags"></i>' + item.WishTag[i] + '</li>');
            }

            addMaskMoney();

            //É necessário abrir o modal antes de carregar as fotos
            //para que elas apareçam...
            $("#homeArea_details").trigger("click");

            homeArea_addSlickImgDetails();

            if (item.UrlPhotos != null && item.UrlPhotos != undefined) {
                
                for (var i = 0; i < item.UrlPhotos.length; i++) {
                    $('#homeArea_detailsMainImage').slick('slickAdd', '<img src="' + item.UrlPhotos[i] + '" alt="Imagem do Produto" title="Imagem do Produto" />');
                    $('#homeArea_detailsImages').slick('slickAdd', '<img src="' + item.UrlPhotos[i] + '" alt="Imagem do Produto" title="Imagem do Produto" />');
                }
            }
        },
        error: function (response) {
            console.log("Erro ao carregar os detalhes do produto.");
        }
    });
}

function showLoader(e) {
    if (e) {
        $('#loader').fadeIn();
    }
    else {
        $('#loader').fadeOut();
    }
}

function resizeNiceScroll(){
    
    $("html").getNiceScroll().resize();
}

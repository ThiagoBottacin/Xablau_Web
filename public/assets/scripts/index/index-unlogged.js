﻿
$(document).ready(function () {

    showLoader(true);
    verificaAuthIndex();

    $('html').i18n({lang: "pt-BR"});

    $('#btnModalLogin').click(function () {
        debugger;
        $('.login').val("").removeClass('border-error');
        $('#dvErroLogin').hide();
    });

    infiniteScroll(getItems);
    getItems(currentPage);

    controlsInicialize();
});


function controlsInicialize() {

    addSlickImgDetails();

    $('.card_produto').expire();
    //Funções dos Cards dos Produtos
    $('.card_produto').livequery(function () {

        var cardProduto = $(this);
        var $details = $(this).find('.fa-eye');

        $details.click(function () {

            detailsProducts(cardProduto.attr('data-id'));
        });
    });

    addMaskMoney();
}

var getItems = function(page) {

    showLoader(true);

    ajaxScroll = $.ajax({
        url: _URL + "items/unloged?page=" + page + "&registers=" + registersPerPage,
        type: 'GET',
    })
        .done(function (data) {

            var items = data.items;

            scrollinitialize(data.totRegisters);

            if(page === 1) $('#masonry').empty();

            for (var i = 0; i < items.length; i++) {

                if (items[i].UrlPhotos == null || items[i].UrlPhotos == "" || items[i].UrlPhotos == undefined) {
                    items[i].UrlPhotos[0] = "./assets/img/800x600.png";
                }

                $("#masonry").append(

                    '<div class="col-md-4 card_produto" data-id="' + items[i].ItemId + '">' +
                    '<div class="product-thumb my-card">' +
                    '<header class="product-header">' +
                    '<img id="productImage' + i + '" src="' + items[i].UrlPhotos[0] + '" alt="Image" title="Imagem" widht="250px" height="250px"/>' +
                    '<div class="product-quick-view">' +
                    '<a id="detalhes-produto" class="fa fa-eye popup-text detalhes-produto" title="Mais Detalhes"></a>' +
                    '</div>' +
                    '<div class="product-secondary-image">' +
                    '<img src="' + items[i].UrlPhotos[0] + '" alt="Image" title="Imagem" widht="250px" height="250px" />' +
                    '</div>' +
                    '</header>' +
                    '<div class="product-inner">' +
                    '<h5 class="product-title" id="productName' + i + '">' + items[i].Name + '</h5>' +
                    '<p class="product-desciption" id="productDescription' + i + '" hidden>' + items[i].Description + '</p>' +
                    '<p class="product-desciption" id="productId' + i + '" hidden>' + items[i].ItemId + '</p>' +
                    '<div class="product-meta">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                );
            }

            addMaskMoney();
        })
        .error(function (data) {
            console.log('load items error')
        })
        .complete(function () {
            
            showLoader(false);
            resizeNiceScroll();
            bindScroll();
        });
}

//Adiciona máscara de dinheiro
function addMaskMoney() {

    //Adiciona mascara de Valor (R$)
    $('.product-price').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
}

//Adiciona plugin slick nas fotos do modal Details
function addSlickImgDetails() {

    //Imagens do modal Detalhes
    $('#detailsMainImage').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#detailsImages'
    });
    $('#detailsImages').not('.slick-initialized').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        asNavFor: '#detailsMainImage'
    });
}

//Monta modal detalhes
function detailsProducts(itemId) {

    //Limpa Campos
    $('#detailsName').val("");
    $('#detailsPrice').val("");
    $('#detailsDescription').val("");
    $('#detailsAddress').val("");

    //Limpa as Tags já inseridas
    $('#detailsTag').empty();

    //Limpa as Fotos já inseridas
    $('#detailsMainImage').slick('unslick');
    $('#detailsImages').slick('unslick');
    $('#detailsMainImage').empty();
    $('#detailsImages').empty();

    $.ajax({
        type: "GET",
        url: _URL + "items/"+itemId+"/details",
        headers: {
            authorization: token
        },
        success: function (response) {

            var item = response[0];

            $('#detailsName').text(item.Name);
            $('#detailsPrice').text(item.Price);
            $('#detailsDescription').text(item.Description);
            $('#detailsAddress').text(item.Address != null && item.Address != undefined ? item.Address : "Endereço não cadastrado");

            for (var i = 0; i < item.WishTag.length; i++) {
                $('#detailsTag').append('<li><i class="fa fa-tags"></i>' + item.WishTag[i] + '</li>');
            }

            addMaskMoney();

            /*É necessário abrir o modal antes de carregar as fotos
            para que elas apareçam*/
            $("#details").trigger("click");

            addSlickImgDetails();

            if (item.UrlPhotos != null) {
                
                for (var i = 0; i < item.UrlPhotos.length; i++) {
                    $('#detailsMainImage').slick('slickAdd', '<img src="' + item.UrlPhotos[i] + '" alt="Imagem do Produto" title="Imagem do Produto" />');
                    $('#detailsImages').slick('slickAdd', '<img src="' + item.UrlPhotos[i] + '" alt="Imagem do Produto" title="Imagem do Produto" />');
                }
            }
        },
        error: function (response) {

        }
    });
}

function showLoader(e) {
    if (e) {
        $('#loader').fadeIn();
    }
    else {
        $('#loader').fadeOut();
    }
}

function searchProduct() {

    var query1 = $('#searchText').val();

    $.ajax({
        url: _URL + "items?query=" + query1,
        dataType: "json",
        success: function (data) {


            $('#searchText').autocomplete({
                maxdata: 5,
                source: function (request, response) {
                    var results = $.ui.autocomplete.filter(data, request.term);

                    response(results.slice(0, 5));
                }
            });
        }
    });


    var query2 = $('#searchLocality').val();

    $.ajax({
        url: _URL + "address?query=" + query2,
        dataType: "json",
        success: function (data) {


            $('#searchLocality').autocomplete({
                maxdata: 5,
                source: function (request, response) {
                    var results = $.ui.autocomplete.filter(data, request.term);

                    response(results.slice(0, 1));
                }
            });
        }
    });


    $("#searchButton").click(function () {
        
        var name = $('#searchText').val();
        var locale = $('#searchLocality').val();
                    
        var registers = 9;
        
        $.ajax({
            url: _URL + "items/search?name=" + name + "&locale=" + locale + "&page=" + 1 + "&registers=" + registers,
            type: 'get',
            data
        })
            .done(function (ressult) {

                console.log(ressult);

            })
            .error(function (ressult) {
                console.log("Erro ao buscar produto");
            });
    });
}

function resizeNiceScroll(){

    $("html").getNiceScroll().resize();
}

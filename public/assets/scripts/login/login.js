﻿
 window.token = localStorage.getItem('xablau');
 window.token = 'Bearer ' + window.token;

$(function () {

    $('.login').focusout(function (e) {
        $(e.target).removeClass('border-error');
    });

    $("#login").click(function () {

        $('.login').removeClass('border-error');
        $('#dvErroLogin').hide();


        var hash = CryptoJS.SHA1($('#loginPassword').val());

        var data = {
            grant_type: 'password',
            username: $('#loginMail').val(),
            password: hash.toString()
        }

        $.ajax({
            url: _URL + "security/token",
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: data
        })
            .done(function (response) {
                localStorage.setItem('xablau', response.access_token);
                window.location.replace("./pages/logged/index-logged.html");
            })
            .error(function (response) {
                $('.login').addClass('border-error');
                $('#dvErroLogin').show().find('label').text(response.responseJSON.error_description);
            })
    });


    $("#register").click(function () {

        var senha1 = $("#registerPassword1").val();
        var senha2 = $("#registerPassword2").val();

        $('#registerPassword1').focusout(function (e) {
            $(e.target).removeClass('border-error');
        });
        $('#registerPassword2').focusout(function (e) {
            $(e.target).removeClass('border-error');
        });
        $('#dvErroPassword').hide();

        $('#registerEmail').focusout(function (e) {
            $(e.target).removeClass('border-error');
        });
        $('#dvErroEmail').hide();

        $('#dvErroEmailExist').hide();

        if (!validateEmail($("#registerEmail").val())) {
            $('#registerEmail').addClass('border-error');
            $('#dvErroEmail').show();
        }

        if (senha1 != senha2) {
            $('#registerPassword1').addClass('border-error');
            $('#registerPassword2').addClass('border-error');
            $('#dvErroPassword').show();
            return;
        }

        if (senha1 == senha2 && validateEmail($("#registerEmail").val())) {

            var hash = CryptoJS.SHA1(senha1);

            var data = {
                PersonId: "0",
                Name: $("#registerName").val(),
                Email: $("#registerEmail").val(),
                Password: hash.toString(),
                DtBorn: $("#registerDate").val()
            }

            $.ajax({
                url: _URL + "user/register",
                type: 'post',
                data: data
            })
                .done(function (data) {
                    $(".mfp-close").trigger("click");
                    swal({
                        title: 'Concluído!',
                        text: 'Borá realizar suas melhores trocas!!',
                        type: 'success',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {

                            var hash = CryptoJS.SHA1($('#registerPassword1').val());

                            var data = {
                                grant_type: 'password',
                                username: $('#registerEmail').val(),
                                password: hash.toString()
                            }

                            $.ajax({
                                url: _URL + "security/token",
                                type: 'post',
                                contentType: 'application/x-www-form-urlencoded',
                                data: data
                            })
                                .done(function (response) {
                                    localStorage.setItem('xablau', response.access_token);
                                    window.location.replace("./pages/logged/index-logged.html");
                                });
                        }
                    });
                })
                .error(function (data) {
                    $('#registerEmail').addClass('border-error');
                    $('#dvErroEmailExist').show();
                });
        }
    });

    $("#logout").click(function () {

        localStorage.removeItem("xablau");
        window.location.replace("../../index.html")
    });
});

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}


function verificaAuthIndex() {

    $.ajax({
        url: _URL + "user/validate",
        type: 'get',
        headers: {
            authorization: window.token
        },
    })
    .done(function (data) {
        window.location.replace("./pages/logged/index-logged.html")
    });
}

function verificaAuth() {

    $.ajax({
        url: _URL + "user/validate",
        type: 'get',
        async: true,
        headers: {
            authorization: window.token
        },
    })
    .success(function (response) {

        $('#nameLoged').empty();
        $('#nameLoged').append(response != null && response != undefined && response != "" ? response.name : "Usuário");
    })
    .error(function (data) {
        swal({
            title: 'Sessão Expirada!',
            text: 'Sua sessão expirou, faça o login novamente para continuar.',
            type: 'error',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false
        }).then(function (isConfirm) {
            if (isConfirm) {
                window.location.replace("../../index.html");
            }
        });
    });
}

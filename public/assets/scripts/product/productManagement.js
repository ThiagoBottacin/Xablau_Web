﻿var removedFiles = [];
var uploadComplete = false;

Dropzone.autoDiscover = false;
var myDropzone = new Dropzone('#dropzone', {
    url: _URL + "upload/files?type=Items",
    headers: {
        authorization: window.token
    },
    uploadMultiple: true,
    maxFiles: 5,
    maxFilesize: 3, // mb
    acceptedFiles: "image/*",
    addRemoveLinks: true,
    autoProcessQueue: false,
    parallelUploads: 5,
    init: function () {
        this.on("queuecomplete", function () {
            saveItem(); 
        });
        this.on('removedfile', function(file){
            
            var itemId = $('#btnSalvar').attr('data-id');
            
            if (itemId !== undefined && file.upload === undefined) {
                
                removedFiles.push(file.name);
            }
        });
        this.on('complete', function(){

            uploadComplete = true;
        });
        
    }
});

$(document).ready(function () {

    debugger;
    if ($('#itemsArea').is(':visible')) {
        
        items_pageLoad();
    }
});


function items_pageLoad() {
    
    verificaAuth();

    showLoader(true);

    $('.itemsArea_dvCardProduto').expire();

    $('.tags').tagsinput({
        maxTags: 10,
        maxChars: 18,
        trimValue: true,
        typeahead: {
            source: function (query) {
                return $.get(_URL + "tags?query=" + query)
            },
            autoSelect: false
        },
        freeInput: true
    });

    $('#addProduct').on('hide', function () {
        clearItemModal();
    });


    //Funções Principais
    infiniteScroll(getItems, 9);
    getItems(currentPage);

    //Adicionar produtos
    $('#btnSalvar').click(function (e) {

        e.preventDefault();

        $('#loader').appendTo('#addProduct');
        showLoader(true);

        if (myDropzone.files.length && !uploadComplete)
            myDropzone.processQueue();
         else 
            saveItem();
    });

    addMaskMoney();
}


function saveItem(){
    
    var item = {
        Id: $('#btnSalvar').attr('data-id'),
        Name: $('#txtNomeProduto').val(),
        Description: $('#txtDescProduto').val(),
        Price: $('#txtPrecoProduto').unmask(),
        DescriptionTag: $('#txtTagDesc').tagsinput('items'),
        WishTag: $('#txtTagDesejo').tagsinput('items'),
        RemovedFiles: removedFiles
    };
    
    var methodType = "POST";
    var msg = "Produto cadastrado com sucesso!";

    if (item.Id > 0) {
        
        methodType = "PUT";
        msg = "Produto editado com sucesso!";
    }

    $.ajax({
        type: methodType,
        url: _URL + "items",
        data: item,
        headers: {
            authorization: window.token
        },
        async: true,
        success: function (response) {

            $('#addProduct').modal('hide');
            setCurrentPage(1);

            swal({

                title: 'Concluído!',
                text: msg,
                type: 'success',
                allowOutsideClick: true,
                allowEscapeKey: true,
                allowEnterKey: true

            }).then(function (isConfirm) {

                if (isConfirm) {
                    
                    getItems(currentPage);
                }
            });
        },
        error: function (response) {
            swal(
                'Oops...',
                response.responseText,
                'error'
            );
        },
        complete: function(){

            showLoader(false);
        }
    });
}



var getItems = function(page) {

    showLoader(true);

    ajaxScroll =  $.ajax({
        type: "GET",
        url: _URL + "items/my-items?page=" + page + "&registers=" + registersPerPage,
        dataType: "json",
        async: true,
        headers: {
            authorization: window.token
        },
        success: function (response) {

            var items = response.items;
            scrollinitialize(response.totRegisters);

            var btnNewItem = $('#btnNewItem').detach();

            if (items.length > 0) {
                $('#itemsArea_dvAlertaProduto').hide();

                var cardProduto = $('#itemCard_Template #itemsArea_dvCardProduto');

                cardProduto.val("");
                cardProduto.find('.product-title').text("");
                cardProduto.find('.product-desciption').text("");
                cardProduto.find('.product-price').val("");

                if (page === 1) $('#itemsArea_dvProdutos').empty();

                for (var i = 0; i < items.length; i++) {

                    var item = items[i];
                    
                    cardProduto.attr('data-id', item.ItemId);
                    cardProduto.find('.product-title').text(item.Name);
                    //cardProduto.find('.product-desciption').text(item.Description);

                    if (item.UrlPhotos != null && item.UrlPhotos != undefined && item.UrlPhotos.length > 0)
                        cardProduto.find('#imgProduto').attr('src', item.UrlPhotos[0]);
                    else
                        cardProduto.find('#imgProduto').attr('src', "../../assets/img/800x600.png");

                    if (item.Price == null || item.Price != "")
                        cardProduto.find('.product-price').text(item.Price).show();
                    else
                        cardProduto.find('.product-price').text("").hide();


                    cardProduto.clone().show().appendTo('#itemsArea_dvProdutos');
                }

                //Adiciona mascara de Valor (R$)
                addMaskMoney();
                addLiveQuery();
            }
            else {
                $('#itemsArea_dvAlertaProduto').show();
            }
            
            btnNewItem.show().appendTo('#itemsArea_dvProdutos');
        },
        error: function (data) {

        },
        complete: function () {

            showLoader(false);
            resizeNiceScroll();
            bindScroll();
        }
    });
}

function addLiveQuery(){

    $('.itemsArea_dvCardProduto').expire();
    //Ações dos cards dos Produtos (Editar e Remover)
    $('.itemsArea_dvCardProduto').livequery(function () {

        var cardProduto = $(this);
        var remover = $(this).find('.btnExcluirProduto');
        var editar = $(this).find('.btnEditarProduto');

        //Editar Produto (Carrega modal com as informações do Item)
        editar.click(function () {
            
            showLoader(true);

            var itemId = cardProduto.attr('data-id');
            loadModalEdit(itemId);
        });

        //Remover Produto
        remover.click(function () {

            var itemId = cardProduto.attr('data-id')

            $.ajax({
                type: "DELETE",
                url: _URL + "items?itemId=" + itemId,
                headers: {
                    authorization: window.token
                },
                async: true,
                success: function (response) {
                    swal({
                        title: 'Concluído!',
                        text: 'Produto removido com sucesso.',
                        type: 'success',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false
                    }).then(function () {

                            cardProduto.remove();
                    });
                },
                    error: function (response) {
                    swal(
                        'Oops...',
                        response.responseText,
                        'error'
                    );
                }
            });
        });
    });
}

function loadModalEdit(itemId){

    $('#btnSalvar').attr('data-id', itemId);

    $.ajax({
        type: "GET",
        url: _URL + "items/"+ itemId +"/details",
        headers: {
            authorization: window.token
        },
        success: function (response) {
            debugger;

            var item = response[0];

            $('#txtNomeProduto').val(item.Name);
            $('#txtDescProduto').val(item.Description);
            $('#txtPrecoProduto').val(item.Price);

            for (var i = 0; i < item.DescriptionTag.length; i++) {
                $('#txtTagDesc').tagsinput('add', item.DescriptionTag[i]);
            }
            for (var i = 0; i < item.WishTag.length; i++) {
                $('#txtTagDesejo').tagsinput('add', item.WishTag[i]);
            }

            addImageDropZone(item.Photos);

            addMaskMoney();

            
            $("#addProduct").modal('show');
        },
        error: function (response) {
            swal(
                'Oops...',
                response.responseText,
                'error'
            );
        },
        complete: function(){

            showLoader(false);
        }
    });
}

function addImageDropZone(images){

    myDropzone.removeAllFiles(true);

    for (let i = 0; i < images.length; i++) {
        const img = images[i];
        
        var file = { 
            name: img.Name, 
            size: img.Size,
        };

        myDropzone.emit("addedfile", file);                                
        myDropzone.emit("thumbnail", file, img.UrlPhoto);
        // myDropzone.files.push(file);
    }
}


function clearItemModal(){
    
    myDropzone.removeAllFiles(true);
    removedFiles = [];

    $('#btnSalvar').attr('data-id', '');
    $('#txtNomeProduto').val("");
    $('#txtDescProduto').val("");
    $('#txtPrecoProduto').val("");
    $('#txtTagDesc').tagsinput('removeAll');
    $('#txtTagDesejo').tagsinput('removeAll');
}

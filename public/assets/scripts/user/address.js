﻿$(document).ready(function () {

    $('.address-box-new').on('click', function(){

        limpa_formulário_cep();
    });

    $('#addAddress').on('click', function () {

        addAddress();
    });

    $('#listaEndereco').on('click', '.address-box-remove', function () {
        
        removeAddress($(this).attr('data-id'))
    });
    
    if ($('#addressesArea').is(':visible')) {

        address_pageLoad();
    }
})

function address_pageLoad() {
    
    showLoader(true);

    removeInfiniteScroll();

    verificaAuth();
    getAddress();
    
    //Quando o campo cep perde o foco.
    $("#cep").blur(function () {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep !== "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                $("#uf").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#uf").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
}


function getAddress() {

    $.ajax({
        url: _URL + "address",
        type: 'GET',
        headers: {
            authorization: window.token
        },
    })
        .done(function (data) {
            
            var cardAddress = $('#cardAddress_Template #cardAddress');
            var btnAddAddress = $('#btnAddAddress').detach();
            
            cardAddress.val("");
            cardAddress.find('.address-box-remove').attr('data-id', "");
            cardAddress.find('.city').text('');
            cardAddress.find('.district').text('');
            cardAddress.find('.street').text('');
            cardAddress.find('.number').text('');
            cardAddress.find('.zipCode').text('');

            $('#listaEndereco').empty();
            for (var i = 0; i < data.length; i++) {

                var item = data[i];

                cardAddress.find('.address-box-remove').attr('data-id', item.AddressId);
                cardAddress.find('.city').text(item.City);
                cardAddress.find('.district').text(item.State);
                cardAddress.find('.street').text(item.Street);
                cardAddress.find('.number').text(item.Number);
                cardAddress.find('.zipCode').text(item.ZipCode);

                cardAddress.clone().show().appendTo('#listaEndereco');
            }
            
            btnAddAddress.show().appendTo('#listaEndereco');

            showLoader(false);
        })
		    .error(function (data) {
		        //TODO
		    });
}


function addAddress() {
    
    if ($("#cidade").val() == "" || $("#uf").val() == "" || $("#bairro").val() == "" || $("#rua").val() == "" || $("#numero").val() == "" || $("#cep").val() == "") {

        swal("Oops...", "Preencha todos os campos!!", "error");

    } else {
        
        var data = {
            AddressId: "0",
            City: $("#cidade").val(),
            State: $("#uf").val(),
            District: $("#bairro").val(),
            Street: $("#rua").val(),
            Number: $("#numero").val(),
            ZipCode: $("#cep").val()
        }

        $.ajax({
            url: _URL + "address/add",
            type: 'POST',
            headers: {
                authorization: window.token
            },
            data
        })
            .done(function (data) {

                var result = data;

                if (result == "Sucess") {
                    $(".mfp-close").trigger("click");
                    swal("Cadastrado!", "Endereço cadastrado com sucesso!", "success")
                        .then(function(){

                            getAddress();
                        });
                }
                else {
                    swal("Oops...", "Endereço inválido!", "error");
                }
            })
            .error(function (data) {
                swal("Oops...", "Endereço inválido!", "error");
            });
    }
}

function removeAddress(addressId) {

    swal({
        title: 'Você tem certeza?',
        text: "O endereço será removido!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sim',
        cancelButtonText: 'Cancelar',
        buttonsStyling: true
    }).then(function (isConfirm) {

        if (isConfirm) {
            
            $.ajax({
                type: 'DELETE',
                url: _URL + "address?addressId=" + addressId,
                headers: {
                    authorization: window.token
                },
                success: function (data) {
    
                    swal('Removido!', 'Endereço removido com sucesso!', 'success')
                    .then(function(){
    
                        getAddress();
                    });
                }
            });
        }
    });
}

function limpa_formulário_cep() {
    
    $("#cep").val("");
    $("#cidade").val("");
    $("#uf").val("");
    $("#rua").val("");
    $("#bairro").val("");
    $("#numero").val("");
}

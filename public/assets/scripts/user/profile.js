﻿$(document).ready(function () {

    if ($('#profileArea').is(':visible')) {
        
        

        profile_pageLoad();
    }
});


function profile_pageLoad(){
    
    showLoader(true);
    removeInfiniteScroll();
    
    verificaAuth();

    getProfile();
    saveChange();
    changePassword();
}

var name;
var email;
var phone;

function getProfile() {

    var data = {
        authorization: token
    }

    $.ajax({
        url: _URL + "user/validate",
        type: 'get',
        headers: {
            authorization: token
        },
    })
    .done(function (data) {
        var result = data;
        $("#name").val(result.name);
        $("#email").val(result.email);
        $("#phone").val(result.phone);
        name = result.name;
        email = result.email;
        phone = result.phone;
    })
    .complete(function (data) {
        
        showLoader(false);
    });
    
}

function changePassword() {
    $("#changePassword").click(function () {

        if ($("#newpassword").val() == $("#newpassword2").val()) {

            var oldhash = CryptoJS.SHA1($("#oldpassword").val());
            var newhash = CryptoJS.SHA1($("#newpassword").val());

            var data = {
                OldPassword: oldhash.toString(),
                NewPassword: newhash.toString()
            }

            $.ajax({
                url: _URL + "user/change-password",
                type: 'post',
                headers: {
                    authorization: token
                },
                data
            })
                .done(function (data) {
                    $(".mfp-close").trigger("click");
                    swal({
                        title: 'Concluído!',
                        text: 'Senha alterada com sucesso!!',
                        type: 'success',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {

                        }
                    });
                })
                .error(function (data) {
                    
                });
        }
    });
}

function saveChange() {
    $("#saveChange").click(function () {

        $('#dvErroEmail').hide();
        $('#email').removeClass('border-error');

        showLoader(true);

        var newname = $("#name").val();
        var newemail = $("#email").val();
        var newddd = $("#ddd").val();
        var newphone = $("#phone").val();

        var changed = false;

        if (newname != name || newemail != email) {

            var data = {
                Name: newname,
                Email: newemail
            }

            $.ajax({
                url: _URL + "user/edit",
                type: 'post',
                headers: {
                    authorization: token
                },
                data
            })
                .done(function (data) {
                    changed = true;
                })
                .error(function (data) {
                    debugger
                    $('#email').addClass('border-error');
                    $('#dvErroEmail').show();
                })
                .complete(function(){

                    showLoader(false);
                });

        }

        if (newphone != phone) {

            var data = {
                DDD: newddd,
                Number: newphone
            }

            $.ajax({
                url: _URL + "phone/alter-phone",
                type: 'post',
                headers: {
                    authorization: token
                },
                data
            })
                .done(function (data) {
                    changed = true;
                })
                .error(function (data) {
                    debugger
                    $('#email').addClass('border-error');
                    $('#dvErroEmail').show();
                }).complete(function(){
                    
                    showLoader(false);
                });

        }

        if (changed == true) {
            swal(
                'Atualizado!',
                'Seus dados foram alterados com sucesso!',
                'success'
            )
        }

    });
}

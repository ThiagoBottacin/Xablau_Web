﻿
var $changeId;
var $itemId;

//Monta modal detalhes
function getXablauDetails(changeId, itemId) {

	showLoader(true);
	verificaAuth();

	xablauDetails_addSlickImgDetails();

	$changeId = changeId;
	$itemId = itemId;

	getMessages(changeId);

	$('#mainImage').slick('unslick');
	$('#images').slick('unslick');
	$('#mainImage').empty();
	$('#images').empty();
	
	$('#detailsName').empty();
	$('#detailsPrice').empty();
	$('#detailsDescription').empty();
	$('#detailsAddress').empty();
	$('#detailsTag').empty();

	$.ajax({
		type: "GET",
		url: _URL + "change/" + changeId + "/details",
		headers: {
			authorization: window.token
		},
		success: function (response) {

			var item = response;

			$('#detailsName').text(item.Name);
			$('#detailsPrice').text(item.Price);
			$('#detailsDescription').text(item.Description);
			$('#detailsAddress').text(item.Address != null && item.Address != undefined ? item.Address : "Endereço não cadastrado");


			xablauDetails_addSlickImgDetails();
			if (item.UrlPhotos.length !== 0) {
				for (var i = 0; i < item.UrlPhotos.length; i++) {
					
					$('#mainImage').slick('slickAdd', '<img src="' + item.UrlPhotos[i] + '" alt="Imagem do Produto" title="Imagem do Produto" />');
					$('#images').slick('slickAdd', '<img src="' + item.UrlPhotos[i] + '" alt="Imagem do Produto" title="Imagem do Produto" />');
				}
			} else {

				$('#mainImage').slick('slickAdd', '<img src="../../assets/img/800x600.png" alt="Imagem do Produto" title="Imagem do Produto" />');
			}


			for (var i = 0; i < item.WishTag.length; i++) {
				$('#detailsTag').append('<li><i class="fa fa-tags"></i>' + item.WishTag[i] + '</li>');
			}

			$('.product-info-price').priceFormat({
				prefix: 'R$ ',
				centsSeparator: ',',
				thousandsSeparator: '.'
			});

		},
		error: function (response) {

		},
		complete: function () {
		    showLoader(false);
		}
	});
}

function xablauDetails_addSlickImgDetails() {

    $('#mainImage').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#images'
	});
	
    $('#images').not('.slick-initialized').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
		centerMode: true,
        focusOnSelect: true,
        asNavFor: '#mainImage'
    });
}


$(function() {

	$('#xablauDetailsArea').on('click', '.to-fora', function(){

		like(false, $itemId, $changeId);
		$('.router[data-target=xablausArea]').click();
	});
	
	$("#sendMessage").click(function () {
		
		debugger; 

		var data = {

			Text: $("#message").val().trim(),
			DataSave: null,
			DataDelete: null,
			PersonId:  null,
			ChangeId: $changeId,
		}

		$.ajax({
			url: _URL + "message",
			type: 'post',
			headers: {
				authorization: window.token
			},
			data
		})
			.done(function (data) {
				
				getMessages($changeId);
				$(".mfp-close").trigger("click");
			})
			.error(function (data) {
				//TODO
			});
	});

}); 
    
function getMessages(changeId) {
	
	$.ajax({
		type: "GET",
		url: _URL + "message?changeId=" + changeId,
		headers: {
			authorization: window.token
		},
		success: function (messages) {
			
			$('#messages').empty();

			for (var i = 0; i < messages.length; i++) {
				
				var message = messages[i];

				if (message.IsOwner) {
					
					$('#messages').append(
						
						'<div class=" col-md-12">' +
								'<div class="comment-author pull-right">' +
									'<img src="../../assets/img/50x50.png" alt="Image Alternative text" title="Gamer Chick" />' +
								'</div>' + 
								'<div class="comment-inner pull-right" style="margin-right: 1%;">' +
									'<span class="comment-author-name">' + message.Name +'</span>' +
								'<p class="comment-content">' + message.Text +'</p>' +
							'</div>' +
						'</div>'
					);

				} else {

					$('#messages').append(
						
						'<div class=" col-md-12">' +
							'<div class="comment-author">' +
								'<img src="../../assets/img/50x50.png" alt="Image Alternative text" title="Gamer Chick" />' +
							'</div>' + 
							'<div class="comment-inner">' +
								'<span class="comment-author-name">' + message.Name +'</span>' +
								'<p class="comment-content">' + message.Text +'</p>' +
							'</div>' +
						'</div>'
					);
				}
			}
		},
		error: function (response) {

		},
		complete: function(){
			showLoader(false);
		}
	});
}

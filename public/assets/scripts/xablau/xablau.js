﻿
$(document).ready(function () {

	if ($('#xablausArea').is(':visible')) {
		
		$('#xablausArea').on('click', '.to-fora', function(){
			
			like(false, $(this).attr('data-id'), $(this).attr('data-change'));
	
			$('.router[data-target=xablausArea]').click();
		});

		xablaus_pageLoad();
	}
});

function xablaus_pageLoad(){

	showLoader(true);
	removeInfiniteScroll();

	verificaAuth();

	getMatch();
}

function getMatch() {

    $.ajax({
        url: _URL + "change/xablau",
        type: 'GET',
        headers: {
            authorization: window.token
        },
    })
		.done(function (data) {

			var cardProduto = $('#xablausArea_dvCardProdutoTemplate #xablausArea_dvCardProduto');
			
			$('#xablausArea_dvProdutos').empty();

		    $('#xablausArea_dvAlertaProduto').show();
		    for (var i = 0; i < data.length; i++) {
		        $('#xablausArea_dvAlertaProduto').hide();
		        var item = data[i];
				
		        //cardProduto.attr('data-id', item.ItemId);
				cardProduto.find('#productDetails').attr('data-change', item.ChangeId);
				cardProduto.find('#productDetails').attr('data-id', item.ItemId);
				cardProduto.find('#productDetails').attr('data-target', "xablauDetailsArea");
				cardProduto.find('#productDetails').addClass('router');
		        cardProduto.find('#productImage').attr('src', item.UrlPhotos != "" ? item.UrlPhotos[0] : "../../assets/img/800x600.png");
				cardProduto.find('#productTitle').text(item.Name);
				
				cardProduto.find('#productPrice').text(item.Price);
				cardProduto.find('.to-fora').attr('data-change', item.ChangeId);
				cardProduto.find('.to-fora').attr('data-id', item.ItemId);

		        cardProduto.clone().show().prependTo('#xablausArea_dvProdutos');
			}
			$('.product-page-meta-price').priceFormat({
				prefix: 'R$ ',
				centsSeparator: ',',
				thousandsSeparator: '.'
			});
		})
		.complete(function(){

			showLoader(false);
		});
}
